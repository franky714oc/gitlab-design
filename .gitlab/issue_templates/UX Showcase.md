<!--
Title should be: UX Showcase {{DATE}}
(e.g. "UX Showcase - March 2nd, 2022")
-->
Hi team :wave:!

We have a UX Showcasing coming up on `ADD_DATE`.

I'll be your host :microphone: `@ADD_HOST_HANDLE`! Please add the topic you'd like to showcase below  and add yourself to the [agenda](https://www.google.com/url?q=https://docs.google.com/document/d/1bJ2lQSFtXxpzNwKcepNlWyQaoyP6fz78gfLX-UfjA7s/edit?usp%3Dsharing&sa=D&source=calendar&ust=1659901033234141&usg=AOvVaw3uZ3GLnZkMeLjQuS1KBYN7) prior to the showcase.

Here's what you need to know:

* Each presentation is ~20 minutes and you should come prepared with sufficient artifacts to tell a story. **You do not need to create any special deliverable (such as slides) in order to present.** You can simply walk everyone through a mock-up in Figma etc.
  * Use your best judgment to determine what will most effectively convey the story: sharing handbook/issues, a few slides, a process diagram, a journey map, a series of mockups, a prototype, etc.
* Please make sure you leave some time for audience questions.

For more info, please refer to the [UX Showcase handbook page](https://about.gitlab.com/handbook/engineering/ux/ux-department-workflow/ux-showcase) and watch the [video recordings](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq89nFXtkVviaIfYQPptwJz) of previous showcases.

## Presenters

1. `@ADD_PRESENTER_HANDLE` - _Showcase title_
1. `@ADD_PRESENTER_HANDLE` - _Showcase title_
1. `@ADD_PRESENTER_HANDLE` - _Showcase title_

Unable to present on this date? Please find another designer to swap with. The `#ux_coworking` channel is a good resource when looking to make a swap.

Questions? Thoughts? Let's discuss it below :point_down:
